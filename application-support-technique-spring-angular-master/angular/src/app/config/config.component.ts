import { FormControl } from '@angular/forms';
import { RegistrationService } from '../registration.service';
import { Router } from '@angular/router';
import { User } from '../user';
import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
import { FeedbackService } from '../feedback.service';

@Component({
  selector: 'app-config',
  templateUrl: './config.component.html',
  styleUrls: ['./config.component.css']
})
export class ConfigComponent implements OnInit{
connect :any;
admins:any;
chefback:any;
cheffront:any;
feedbacks:any;
  nom= new FormControl('');
  prenom= new FormControl('');
  username= new FormControl('');
  password= new FormControl('');
  tel=new FormControl('');
  role=new FormControl('');
admin = new User();

  constructor(private registrServ : RegistrationService, private router: Router, private auth : AuthService, private feedserv: FeedbackService) { }

  ngOnInit() {
    this.connect=this.auth.connectedUser;
    console.log(this.connect);
    this.getAdmin();
    this.getFront();
  this.getBack();
  }

  registre(){
    this.admin.nom=this.nom.value;
    this.admin.prenom=this.prenom.value;
    this.admin.username=this.username.value;
    this.admin.password=this.password.value;
    this.admin.tel=this.tel.value;
    this.admin.role=this.role.value;
    console.log(this.admin);

this.registrServ.registrer(this.admin)
.subscribe(
  (res: any) => {
    console.log("add admin");
    this.router.navigate(['../config']);
  },
  err => {
    console.log ("erreur");
  }
)  }


  getAdmin(){
    this.auth.getAll()
    .subscribe(
      (res: any) => {

        this.admins=res;
        console.log(res);
        this.router.navigate(['../config']);

      },
      err => {
        console.log ("erreur");
      }
    )  }


    getFront(){
       this.auth.getAllTech_front()
    .subscribe(
      (res: any) => {

        this.cheffront=res;
        console.log(res);
        this.router.navigate(['../config']);

      },
      err => {
        console.log ("erreur");
      }
    )
    }

    getBack() {
    this.auth.getAllTech_back()
    .subscribe(
      (res: any) => {

        this.chefback=res;
        console.log(res);
        this.router.navigate(['../config']);

      },
      err => {
        console.log ("erreur");
      }
    )  }




}