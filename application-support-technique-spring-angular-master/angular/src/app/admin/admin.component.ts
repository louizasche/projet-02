import { Component, OnInit } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { AuthService } from '../auth.service';
import { Router } from '@angular/router';
import { Feedback } from '../feedback';
import { FeedbackService } from '../feedback.service';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.css']
})
export class AdminComponent implements OnInit {
  fronts:Feedback;
  backs:Feedback;
  connect:any;
  reponseback= new FormControl('', Validators.required);
  reponsefront= new FormControl('', Validators.required);
  id_reclamationf= new FormControl('', Validators.required);
  id_reclamationb= new FormControl('', Validators.required);
  constructor(private feedServ : FeedbackService, private auth : AuthService, private router : Router) { }

  ngOnInit() {
    this.connect=this.auth.connectedUser;
    console.log(this.connect);
        this.getAllfront();
        this.getAllBack();
        
      }
   
    
      getAllfront(){
        this.feedServ.getAllFront()
        .subscribe(
          (res: any) => {
            this.fronts=res;
            console.log(this.fronts);
          },
          err => {
            console.log ("erreur");
          }
        )  }
    
    
      getAllBack(){
      this.feedServ.getAllback()
      .subscribe(
        (res: any) => {
          this.backs=res;
          console.log(this.backs);
        },
        err => {
          console.log ("erreur");
        }
      )  }
    
      ajoutePVB(){
console.log(this.reponseback.value,this.id_reclamationb.value);
this.feedServ.affectReponse(this.id_reclamationb.value,this.reponseback.value)
.subscribe(
  (res: any) => {
    console.log("pv affectée");
  },
  err => {
    console.log ("erreur");
  }
) 
      }
      ajoutePVF(){
        console.log(this.reponsefront.value,this.id_reclamationf.value);
        this.feedServ.affectReponse(this.reponsefront.value,this.id_reclamationf.value)
        .subscribe(
          (res: any) => {
            console.log("pv affectée");
          },
          err => {
            console.log ("erreur");
          }
        ) 
      }
  
}
