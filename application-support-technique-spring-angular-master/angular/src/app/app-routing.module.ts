import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { ProfilComponent } from './profil/profil.component';
import { RegistreComponent } from './registre/registre.component';
import { SignInComponent } from './sign-in/sign-in.component';
import { ReclamationCComponent } from './reclamation-c/reclamation-c.component';
import { ReclamationAComponent } from './reclamation-a/reclamation-a.component';
import { ConfigComponent } from './config/config.component';
import { AdminComponent } from './admin/admin.component';
import {GuardGuard} from './guard.guard';
import { ChatComponent } from './chat/chat.component';
import { ChatCComponent } from './chat-c/chat-c.component';


const routes: Routes = [
  {'path':"registre", component:RegistreComponent},
  {'path':"signIn", component:SignInComponent},
  {'path':"home", component:HomeComponent, canActivate:[GuardGuard]},
  {'path':"profil", component:ProfilComponent, canActivate:[GuardGuard]},
  {'path':"reclamationC", component:ReclamationCComponent, canActivate:[GuardGuard]},
  {'path':"reclamationA", component:ReclamationAComponent, canActivate:[GuardGuard]},
  {'path':"config", component:ConfigComponent, canActivate:[GuardGuard]},
  {'path':"admin", component:AdminComponent, canActivate:[GuardGuard]},
  {'path':"chat", component:ChatComponent, canActivate:[GuardGuard]},
  {'path':"chatC", component:ChatCComponent, canActivate:[GuardGuard]},

  {path: '**', component: SignInComponent }

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
