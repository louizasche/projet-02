import { Component, OnInit } from '@angular/core';
import { User } from '../user';
import { FormControl } from '@angular/forms';
import { RegistrationService } from '../registration.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-registre',
  templateUrl: './registre.component.html',
  styleUrls: ['./registre.component.css']
})
export class RegistreComponent implements OnInit {
  nom= new FormControl('');
  prenom= new FormControl('');
  username= new FormControl('');
  password= new FormControl('');
  tel=new FormControl('');
    email=new FormControl('');

 // image=new FormControl('');
user = new User();
//imageUrl: string = "/assets/images/default-avatar.png";
//fileToUpload: File = null;
  constructor(private registrServ : RegistrationService, private router: Router) { }

  ngOnInit() {
  }
  /*handleFileInput(file: FileList) {
    this.fileToUpload = file.item(0);

    //Show image preview
    var reader = new FileReader();
    reader.onload = (event:any) => {
      this.imageUrl = event.target.result;
      console.log(this.imageUrl);
     

    }
    reader.readAsDataURL(this.fileToUpload);
    console.log(this.fileToUpload);

  }*/

  registre(){
    this.user.nom=this.nom.value;
    this.user.prenom=this.prenom.value;
    this.user.username=this.username.value;
    this.user.password=this.password.value;
    this.user.tel=this.tel.value;
    this.user.role="Client";
    this.user.email=this.email.value;
    console.log(this.user);

this.registrServ.registrer(this.user)
.subscribe(
  (res: any) => {
    console.log("registration");
      this.router.navigate(['/signIn']);
  },
  err => {
    this.router.navigate(['/registre']);
    console.log ("erreur");
  }
)  }
}
