import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders, HttpBackend} from '@angular/common/http'
import {Observable} from 'rxjs'
import { Router } from '@angular/router';
import { User } from './user';

const httpOptions={ headers : new HttpHeaders({'Content-Type' : 'application/json'})};
const apiUrl="http://localhost:5000/user/registration";

@Injectable({
  providedIn: 'root'
})
export class RegistrationService {
  constructor(private http: HttpClient, private _router: Router, private handler : HttpBackend) { 
    this.http=new HttpClient(this.handler); }


    registrer(user : User ): Observable<User>{
  return this.http.post<User>(apiUrl,user,httpOptions);
}


  }