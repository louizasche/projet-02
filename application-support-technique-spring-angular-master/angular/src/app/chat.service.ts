import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ChatService {

  constructor(private httpClient: HttpClient) { }
  getConversation(id_user1 : any, id_user2 : any) {
    const header = new HttpHeaders().set('Authorization', 'Bearer ' + localStorage.getItem('token'));
    return this.httpClient.get('http://localhost:5000/conversation/getOneConversation/' + id_user1 + '/' + id_user2, {headers: header});
  }
  sendMessage(message : any, id_user:any, idConv:any) {
    const header = new HttpHeaders().set('Authorization', 'Bearer ' + localStorage.getItem('token'));
    return this.httpClient.post('http://localhost:5000/message/sendMessage/' + id_user + '/' + idConv, message, {headers: header});
  }
}
