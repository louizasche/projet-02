import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders, HttpBackend} from '@angular/common/http'
import {Observable} from 'rxjs'
import {User} from './user'
import { Router } from '@angular/router';


const httpOptions={ headers : new HttpHeaders({'Content-Type': 'application/json'})
};

const httpOptions1={ headers :new HttpHeaders().append( 'Authorization' , `Bearer ${localStorage.getItem('token')}` ) };

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  connectedUser: User;
  id_user : any;
  role:any;
  constructor(private http: HttpClient, private handler : HttpBackend , private router:Router   ) { 
    this.http=new HttpClient(this.handler);
    this.connectedUser= JSON.parse(localStorage.getItem('loggedUser'));
 
  }


login(user : User ): Observable<User>{
  const apiUrl="http://localhost:5000/user/login";

  return this.http.post<User>(apiUrl,user,httpOptions) ;
}

getToken(){
return localStorage.getItem('token');
}

loggedIn(){
return !! localStorage.getItem('token')
}

logOut(){
  localStorage.clear();
this.connectedUser = null;
this.router.navigate(['../signIn']);
window.location.reload(true);

}
modifierUser(user){
  console.log(user);
  const apiUrl111="http://localhost:5000/user/modification";

  return this.http.post<User>(apiUrl111,user,httpOptions1) ;


}
getByusername( username: String): Observable<String> {
  console.log(username);
  const apiUrl1="http://localhost:5000/user/username";

  return this.http.post<String>(apiUrl1,username,httpOptions1) ;
}

getUserById(id_user:any){
  const apiUrl3="http://localhost:5000/user/one/"+id_user;

  return this.http.get(apiUrl3,httpOptions1)
}
getUserByRole(role:any){
  const apiUrl4="http://localhost:5000/user/role/"+role;

  return this.http.get(apiUrl4,httpOptions1)
}
  getAll(){
    const apiUrl5="http://localhost:5000/user/all";

    return this.http.get(apiUrl5,httpOptions1)
  }
  getAllTech_front(){
    const apiUrl6="http://localhost:5000/user/allTech_front";

    return this.http.get(apiUrl6,httpOptions1)

  }
  getAllTech_back(){
    const apiUrl7="http://localhost:5000/user/allTech_back";

    return this.http.get(apiUrl7,httpOptions1)

  }
  getAllClient(){
    const apiUrl10="http://localhost:5000/user/allClient";

    return this.http.get(apiUrl10,httpOptions1)

  }


}
