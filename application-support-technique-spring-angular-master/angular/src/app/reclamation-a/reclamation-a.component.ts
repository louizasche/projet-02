import { Component, OnInit } from '@angular/core';
import { FeedbackService } from '../feedback.service';
import { Feedback } from '../feedback';
import { Router } from '@angular/router';
import { AuthService } from '../auth.service';
import { FormControl, Validators, NgModel } from '@angular/forms';
import { PipePipe } from '../pipe.pipe';

@Component({
  selector: 'app-reclamation-a',
  templateUrl: './reclamation-a.component.html',
  styleUrls: ['./reclamation-a.component.css']
})
export class ReclamationAComponent implements OnInit  {
   search: any = {etat: '', niveau: ''};

  reclamation :Feedback;
  fronts:Feedback;
    filteredFronts:Feedback;
    filteredBacks:Feedback;
filteredAll: Feedback;
  backs:Feedback;
  connect:any;
  id_tech_back= new FormControl('', Validators.required);
  id_recb= new FormControl('', Validators.required);
  id_tech_front= new FormControl('', Validators.required);
  id_recf= new FormControl('', Validators.required);
  techfront: any;
  techback: any;
  constructor(private feedServ : FeedbackService, private router:Router, private auth: AuthService) { }
 //filter
  setNiveau(value){
    
   this.search.niveau=value;
   console.log(this.search)
   const p = new PipePipe();
   if(this.connect.role=="admin"){
    this.filteredAll = p.transform(this.reclamation, this.search);

   }
   else if(this.connect.role=="Chef-Back"){
    this.filteredBacks = p.transform(this.backs, this.search);

   }
   else if(this.connect.role=="Chef-Front") { 
    this.filteredFronts = p.transform(this.fronts, this.search);

  }

  }
  setEtat(value){
  this.search.etat=value;
  console.log(this.search)
  const p = new PipePipe();
  if(this.connect.role=="admin"){
    this.filteredAll = p.transform(this.reclamation, this.search);

   }
   else if(this.connect.role=="Chef-Back"){
    this.filteredBacks = p.transform(this.backs, this.search);

   }
   else if(this.connect.role=="Chef-Front") { 
    this.filteredFronts = p.transform(this.fronts, this.search);

  } 
  }
//filter

  ngOnInit() {
this.connect=this.auth.connectedUser;
console.log(this.connect);
    this.hetRec();
    this.getAllfront();
    this.getAllBack();

    this.getBack();
    this.getFront();
  }
hetRec(){
  this.feedServ.getAll()
  .subscribe(
    (res: any) => {
      this.reclamation=res;
      this.filteredAll=res;
      console.log(this.reclamation);
      this.router.navigate(['../reclamationA']);
    },
    err => {
      console.log ("erreur");
    }
  )  }

  getAllfront(){
    this.feedServ.getAllFront()
    .subscribe(
      (res: any) => {
        this.fronts=res;
        this.filteredFronts = res;
        console.log(this.fronts);
        this.router.navigate(['../reclamationA']);
      },
      err => {
        console.log ("erreur");
      }
    )  }


  getAllBack(){
  this.feedServ.getAllback()
  .subscribe(
    (res: any) => {
      this.backs=res;
      this.filteredBacks=res;
      console.log(this.backs);
      this.router.navigate(['../reclamationA']);
    },
    err => {
      console.log ("erreur");
    }
  )  }


  getFront(){
    this.auth.getAllTech_front()
 .subscribe(
   (res: any) => {

     this.techfront=res;
     console.log(res);

   },
   err => {
     console.log ("erreur");
   }
 )
 }

 getBack() {
 this.auth.getAllTech_back()
 .subscribe(
   (res: any) => {

     this.techback=res;
     console.log(res);

   },
   err => {
     console.log ("erreur");
   }
 )  }


  affecterTechF(){
    console.log(this.id_tech_front.value,this.id_recf.value);
this.feedServ.affectationTech_Front(this.id_recf.value,this.id_tech_front.value)
.subscribe(
  (res: any) => {

    console.log("affectation succée");

  },
  err => {
    console.log ("erreur");
  }
) 
  }

  affecterTechB(){
console.log(this.id_tech_back.value,this.id_recb.value);
this.feedServ.affectationTech_back(this.id_recb.value,this.id_tech_back.value)

.subscribe(
  (res: any) => {

    console.log("affectation succée");

  },
  err => {
    console.log ("erreur");
  }
) 
  }

}


