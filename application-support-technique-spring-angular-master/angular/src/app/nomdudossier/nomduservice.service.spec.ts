import { TestBed } from '@angular/core/testing';

import { NomduserviceService } from './nomduservice.service';

describe('NomduserviceService', () => {
  let service: NomduserviceService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(NomduserviceService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
