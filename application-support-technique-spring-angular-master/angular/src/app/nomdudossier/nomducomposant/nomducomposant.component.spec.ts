import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NomducomposantComponent } from './nomducomposant.component';

describe('NomducomposantComponent', () => {
  let component: NomducomposantComponent;
  let fixture: ComponentFixture<NomducomposantComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NomducomposantComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NomducomposantComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
