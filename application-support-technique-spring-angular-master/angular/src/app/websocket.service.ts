import { Injectable } from '@angular/core';
import * as SockJs from 'sockjs-client';
import * as Stomp from 'stompjs';

@Injectable({
  providedIn: 'root'
})
export class WebsocketService {
  constructor() { }
    //Open connection with the back-end socket
   public connect() {
    const socket = new SockJs(`http://localhost:5000/socket`);
    const stompClient = Stomp.over(socket);
    stompClient.connect({}, function (frame) {
      console.log('connectionSuccess')
      //_this.stompClient.reconnect_delay = 2000;
  }, this.errorCallBack);
    return stompClient;
}
errorCallBack(error) {
  console.log("errorCallBack -> " + error)
  setTimeout(() => {
      this.connect();
  }, 5000);
}
}
