import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MDBBootstrapModule } from 'angular-bootstrap-md';
import { HomeComponent } from './home/home.component';
import { NavbarComponent } from './navbar/navbar.component';
import { ProfilComponent } from './profil/profil.component';
import { RegistreComponent } from './registre/registre.component';
import { SignInComponent } from './sign-in/sign-in.component';
import { ReclamationCComponent } from './reclamation-c/reclamation-c.component';
import { ReclamationAComponent } from './reclamation-a/reclamation-a.component';
import { ReactiveFormsModule,FormsModule } from '@angular/forms';
import { TokenService } from '../app/token.service';
import { HttpClientModule} from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations'
import { DropdownModule, CollapseModule, CheckboxModule, WavesModule,ButtonsModule, InputsModule, IconsModule, CardsModule  } from 'angular-bootstrap-md';
import { AdminComponent } from './admin/admin.component';
import { ConfigComponent } from './config/config.component';
import { PipePipe } from './pipe.pipe';
import { ChatComponent } from './chat/chat.component';
import { ChatCComponent } from './chat-c/chat-c.component';
import { NomducomposantComponent } from './nomdudossier/nomducomposant/nomducomposant.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    NavbarComponent,
    ProfilComponent,
    RegistreComponent,
    SignInComponent,
    ReclamationCComponent,
    ReclamationAComponent,
    AdminComponent,
    ConfigComponent,
    PipePipe,
    ChatComponent,
    ChatCComponent,
    NomducomposantComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
     MDBBootstrapModule,
     FormsModule,
     ReactiveFormsModule,
     HttpClientModule ,
     BrowserAnimationsModule,
     CollapseModule,
     DropdownModule.forRoot() ,
     CheckboxModule, 
     WavesModule.forRoot(), 
     ButtonsModule, 
     InputsModule, 
     IconsModule, 
     CardsModule,
     CardsModule.forRoot()
  ],
  providers: [TokenService],
  bootstrap: [AppComponent]
})
export class AppModule { }
