package projet.controllers;

import java.util.List;

import javax.persistence.EntityManager;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import projet.entity.Feedback;
import projet.repository.FeedbackRepository;
import projet.services.FeedbackService;

@RestController
@CrossOrigin("*")
@RequestMapping("/reclamation")
public class FeedbackControllers {

	@Autowired
	FeedbackService feedbackservice;
	@Autowired
	FeedbackRepository feedbackrepo;
	@Autowired
	EntityManager em;
	
	@RequestMapping(value = "/addReclamation/{id_user}", method = RequestMethod.POST)
	public void ajouterFeedback(@PathVariable("id_user") int id_user ,@RequestBody Feedback feedback) {
		System.out.println(id_user);
		feedbackservice.ajouterFeedback(feedback, id_user);
	}
	
	@RequestMapping(value = "/delete", method = RequestMethod.POST)
	public void supprimerFeedbackById(@RequestBody int id_rec) {
	feedbackservice.supprimerFeedbackById(id_rec);
	}
	
    @RequestMapping(value = "/all", method = RequestMethod.GET)
    public List<Feedback> getallFeedback() {
    
    	return feedbackservice.getallFeedback();
    }
    
    @RequestMapping(value = "/allByClient", method = RequestMethod.POST)
    public List<Feedback> getallFByUser(@RequestBody int id_user) {
    
    	return feedbackservice.getallFeedbackByIdClient(id_user);
    }
    
    @RequestMapping(value = "/allByChef_back", method = RequestMethod.POST)
    public List<Feedback> getallFByChef_back(@RequestBody int id_user) {
    
    	return feedbackservice.getallFeedbackByIdChef_back(id_user);
    }
    
    @RequestMapping(value = "/allByChef_front", method = RequestMethod.POST)
    public List<Feedback> getallFByChef_front(@RequestBody int id_user) {
    
    	return feedbackservice.getallFeedbackByIdChef_front(id_user);
    }
    
    @RequestMapping(value = "/allByTech_back", method = RequestMethod.POST)
    public List<Feedback> getallFByTech_back(@RequestBody int id_user) {
    
    	return feedbackservice.getallFeedbackByIdTECH_back(id_user);
    }
    
    @RequestMapping(value = "/allByTech_front", method = RequestMethod.POST)
    public List<Feedback> getallFByTech_front(@RequestBody int id_user) {
    
    	return feedbackservice.getallFeedbackByIdTech_front(id_user);
    }
    
    @RequestMapping(value = "/one/{id_rec}", method = RequestMethod.GET)
	public Feedback getFeedbackById(@PathVariable("id_rec") int id) {
    return feedbackservice.getFeedbackById(id);
    }
	
    @RequestMapping(value = "/affectationTech_back/{idtech_back}/{id_rec}", method = RequestMethod.GET)
	public void affeTech_back (@PathVariable("idtech_back") int idtech_back,@PathVariable("id_rec") int id_rec) {
    	feedbackservice.affeTech_back(idtech_back, id_rec);
    }
    
    @RequestMapping(value = "/affectationTech_Front/{idtech_front}/{id_rec}", method = RequestMethod.GET)
	public void affeTech_front (@PathVariable ("idtech_front") int idtech_front,@PathVariable("id_rec") int id_rec) {
    	feedbackservice.affeTech_front(idtech_front, id_rec);
    }
    
    @RequestMapping(value = "/type/{type}", method = RequestMethod.GET)
	public Feedback getFeedbackBType(@PathVariable("type") String type) {
    return feedbackservice.getFeedbackByType(type);
    }
    
    @RequestMapping(value = "/AllBytype/{type}", method = RequestMethod.GET)
   	public List<Feedback> getAllFeedbackBType(@PathVariable("type") String type) {
       return feedbackservice.getAllFeedbackByType(type);
       } 
    
    @RequestMapping(value = "/reponse/{id_rec}", method = RequestMethod.POST)
    public void ajoutReponse(@PathVariable("id_rec") int id_rec, @RequestBody String reponse) {
     feedbackservice.ajoutReponse(reponse, id_rec);
    
    }

    
}
