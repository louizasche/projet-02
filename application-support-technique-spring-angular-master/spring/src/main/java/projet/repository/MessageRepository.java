package projet.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import projet.entity.Message;


@Repository("messagerepository")
public interface MessageRepository extends JpaRepository<Message, Integer> {

}
