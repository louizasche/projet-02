package projet.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import projet.entity.Conversation;


@Repository("conversationrepository")
public interface ConversationRepository extends JpaRepository<Conversation, Integer>  {
}
