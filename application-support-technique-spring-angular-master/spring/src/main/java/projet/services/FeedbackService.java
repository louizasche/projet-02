package projet.services;

import java.util.List;

import projet.entity.Feedback;

public interface FeedbackService {
	void ajouterFeedback(Feedback feedback, int id_user);
	void supprimerFeedbackById(int id_rec);
	public  List<Feedback> getallFeedback();
	public  List<Feedback> getallFeedbackByIdClient(int id_user);
	public  List<Feedback> getallFeedbackByIdChef_back(int id_user);
	public  List<Feedback> getallFeedbackByIdChef_front(int id_user);
	public  List<Feedback> getallFeedbackByIdTECH_back(int id_user);
	public  List<Feedback> getallFeedbackByIdTech_front(int id_user);
	public List<Feedback> getAllFeedbackByType(String type) ;
	public Feedback getFeedbackById(int id_rec);
	public Feedback getFeedbackByType(String type);

	public void affeTech_back (int idtech_back, int id_rec);
	public void affeTech_front (int idtech_front, int id_rec);
    public void ajoutReponse(String reponse, int id_rec);
}
