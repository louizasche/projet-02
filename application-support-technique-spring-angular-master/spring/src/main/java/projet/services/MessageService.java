package projet.services;

import projet.entity.Message;

public interface MessageService {
	public void sendMessage(Message message);

}
