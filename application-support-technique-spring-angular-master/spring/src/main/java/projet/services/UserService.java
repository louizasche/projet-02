package projet.services;

import java.util.List;

import org.springframework.stereotype.Repository;

import projet.entity.User;

@Repository("UserRepository")
public interface UserService {
		void ajouterUser(User user);
		void supprimerUserByID(int id_user);
		void modifier(User user);
	public  List<User> getallUser();
	public User getUserById(int id_user); 
	public User getByUsername(String username);
	public User getByRole(String role);
	public  List<User> getAllTech_back();
	public  List<User> getAllTech_front();
	public  List<User> getAllClient();
	

}
