package projet.services;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import projet.entity.User;
import projet.repository.UserRepository;

@Service("userProjServices")
public class UserServiceimpl implements UserService {

    @Autowired
    UserRepository userRepository;
	@PersistenceContext
	EntityManager em;
	
	@Override
	public User getByUsername(String username) {
		TypedQuery<User> query = (TypedQuery<User>) em.createQuery("SELECT u FROM User u WHERE u.username = :username" ,User.class);
		User u=query.setParameter("username", username).getSingleResult();
			return  u;	
	}
	
	@Override
	public User getByRole(String role) {
		TypedQuery<User> query = (TypedQuery<User>) em.createQuery("SELECT u FROM User u WHERE u.role = :role" ,User.class);
		User u=query.setParameter("role", role).getSingleResult();
			return  u;
	}
	@Transactional
	@Override
	public void ajouterUser(User user) {
		userRepository.save(user);
	}

	@Override
	public void supprimerUserByID(int id_user) {
	User user=	em.find(User.class, id_user);
		em.remove(user);
	}
	@Override
	public void modifier(User user) {
	User exist=	em.find(User.class, user.getId_user());
	
	exist.setPassword(user.getPassword());
	

		em.merge(user);
	}
	
	@Override
	public List<User> getallUser() {
		return userRepository.findAll();
	}

	@Override
	public User getUserById(int id_user) {
		return userRepository.getOne(id_user);
	}
	
	@Override
	public  List<User> getAllTech_back()  {
		String role ="Tech-Back";
		TypedQuery<User> query = (TypedQuery<User>) em.createQuery("SELECT u FROM User u WHERE u.role = :role" ,User.class);
		 List<User>  u=query.setParameter("role",role).getResultList();
			return  u;
	}
	@Override
	public  List<User> getAllTech_front()  {
		String role ="Tech-Front";
		TypedQuery<User> query = (TypedQuery<User>) em.createQuery("SELECT u FROM User u WHERE u.role = :role" ,User.class);
		 List<User>  u=query.setParameter("role",role).getResultList();
			return  u;
	}
	
	@Override
	public  List<User> getAllClient() {
		String role ="Client";
		TypedQuery<User> query = (TypedQuery<User>) em.createQuery("SELECT c FROM User c WHERE c.role = :a" ,User.class);
		 List<User>  c=query.setParameter("a",role).getResultList();
			return  c;
	}

	
	
}
