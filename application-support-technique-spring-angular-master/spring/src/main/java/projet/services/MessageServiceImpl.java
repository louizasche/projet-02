package projet.services;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import projet.entity.Message;
import projet.repository.MessageRepository;
import projet.services.MessageService;

@Service("messageservice")
public class MessageServiceImpl implements MessageService{
	@Autowired
	MessageRepository messagerepository;
	
	@PersistenceContext
	EntityManager em;
	@Override
	public void sendMessage(Message message) {
		messagerepository.save(message);
	}


}
