package projet.entity;


import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class Feedback {

	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id_rec;
	private String description;
	private String reponse;
	private String type;
	private String etat;
	private String niveau;
	
	
	@ManyToOne
	private User client;
	@ManyToOne
	private User admin;

	@ManyToOne
	private User chef_back;
	@ManyToOne
	private User chef_front;
	@ManyToOne
	private User tech_back;
	@ManyToOne
	private User tech_front;

	

	
	public User getAdmin() {
		return admin;
	}

	public void setAdmin(User admin) {
		this.admin = admin;
	}

	public User getClient() {
		return client;
	}

	public void setClient(User client) {
		this.client = client;
	}
	
	
	public User getChef_back() {
		return chef_back;
	}

	public void setChef_back(User chef_back) {
		this.chef_back = chef_back;
	}
	
	public User getChef_front() {
		return chef_front;
	}
	public void setChef_front(User chef_front) {
		this.chef_front = chef_front;
	}
	
	
	public User getTech_back() {
		return tech_back;
	}

	public void setTech_back(User tech_back) {
		this.tech_back = tech_back;
	}
	
	public User getTech_front() {
		return tech_front;
	}

	public void setTech_front(User tech_front) {
		this.tech_front = tech_front;
	}

	public Feedback() {}


	


	


	

	

	public Feedback( String type, String etat, String niveau, String description) {
		super();
		this.description = description;
		this.type = type;
		this.etat = etat;
		this.niveau = niveau;
	}

	public Feedback(int id_rec, String description, String reponse, String type, String etat, String niveau,
			User client, User admin, User chef_back, User chef_front, User tech_back, User tech_front) {
		super();
		this.id_rec = id_rec;
		this.description = description;
		this.reponse = reponse;
		this.type = type;
		this.etat = etat;
		this.niveau = niveau;
		this.client = client;
		this.admin = admin;
		this.chef_back = chef_back;
		this.chef_front = chef_front;
		this.tech_back = tech_back;
		this.tech_front = tech_front;
	}

	public int getId_rec() {
		return id_rec;
	}


	public void setId_rec(int id_rec) {
		this.id_rec = id_rec;
	}


	public String getDescription() {
		return description;
	}


	public void setDescription(String description) {
		this.description = description;
	}


	public String getReponse() {
		return reponse;
	}


	public void setReponse(String reponse) {
		this.reponse = reponse;
	}


	public String getType() {
		return this.type;
	}


	public void setType(String type) {
		this.type = type;
	}


	public String getEtat() {
		return etat;
	}


	public void setEtat(String etat) {
		this.etat = etat;
	}


	public String getNiveau() {
		return niveau;
	}


	public void setNiveau(String niveau) {
		this.niveau = niveau;
	}
	
	
	
}
